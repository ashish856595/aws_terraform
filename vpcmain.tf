// Creating VPC
resource "aws_vpc" "main" {
  cidr_block = var.cidr.vpcrange
  tags = {
    Name = var.resources.vpc-name
  }
}
// Private Subnet-1
resource "aws_subnet" "subnet-private-1" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.cidr.priv-sub-1-cidr
  availability_zone = var.location.aws_az1
  tags = {
    Name = var.resources.subnet-private-1
  }
}
// Private Subnet-2
resource "aws_subnet" "subnet-private-2" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.cidr.priv-sub-2-cidr
  availability_zone = var.location.aws_az2
  tags = {
    Name = var.resources.subnet-private-2
  }
}
// Public Subnet-1
resource "aws_subnet" "subnet-public-1" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.cidr.pub-sub-1-cidr
  map_public_ip_on_launch = "true"
  availability_zone       = var.location.aws_az1
  tags = {
    Name = var.resources.subnet-public-1
  }
}
// Public Subnet-2
resource "aws_subnet" "subnet-public-2" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.cidr.pub-sub-2-cidr
  map_public_ip_on_launch = "true"
  availability_zone       = var.location.aws_az2
  tags = {
    Name = var.resources.subnet-public-2
  }
}
// Creating IGW
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = var.resources.igw
  }
}
// Creating EIP
resource "aws_eip" "nat" {
  #EIP may require igw to exist prior to association
  #Use depends_on to set an explicit dependency on the igw.
  depends_on = [aws_internet_gateway.igw]
  tags = {
    Name = var.resources.elastic-ip
  }
}
// Creating NAT gateway
resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.subnet-public-1.id
  tags = {
    Name = var.resources.nat
  }
}
// Creating Custom Private Route Table and attaching NAT to it.
resource "aws_route_table" "priv-rt" {
  vpc_id = aws_vpc.main.id
  route {
    //associated subnet can reach everywhere
    cidr_block = var.cidr.dest_cidr_block
    gateway_id = aws_nat_gateway.nat.id
  }
  tags = {
    Name = var.resources.priv-rt
  }
}
// Associating Subnets to Private Route Table 
resource "aws_route_table_association" "subnet-private-1" {
  subnet_id      = aws_subnet.subnet-private-1.id
  route_table_id = aws_route_table.priv-rt.id
}
resource "aws_route_table_association" "subnet-private-2" {
  subnet_id      = aws_subnet.subnet-private-2.id
  route_table_id = aws_route_table.priv-rt.id
}
// Creating Custom Public Route Table and attaching IGW to it.
resource "aws_route_table" "pub-rt" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = var.cidr.dest_cidr_block    # Associated subnet can reach everywhere
    gateway_id = aws_internet_gateway.igw.id # RT uses this IGW to reach internet
  }
  tags = {
    Name = var.resources.pub-rt
  }
}
// Associating Subnets to Public Route Table 
resource "aws_route_table_association" "subnet-public-1" {
  subnet_id      = aws_subnet.subnet-public-1.id
  route_table_id = aws_route_table.pub-rt.id
}
resource "aws_route_table_association" "subnet-public-2" {
  subnet_id      = aws_subnet.subnet-public-2.id
  route_table_id = aws_route_table.pub-rt.id
}

# Creating Security group for Bastion Host
resource "aws_security_group" "sg_bastion_host" {
  name        = "bastion-host"
  description = "Allow ssh to bastion host"
  vpc_id      = aws_vpc.main.id
  ingress {
    description = "allow ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["171.48.0.144/32"]
  }
  tags = {
    Name = var.security_group.sg_bh
  }
}

# Security group for Private Instances
resource "aws_security_group" "sg_private_instance" {
  name        = "sg_private_instance"
  description = "Allow ssh for private instance"
  vpc_id      = aws_vpc.main.id
  ingress {
    description     = "allow ssh"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.sg_bastion_host.id]
    #cidr_blocks = ["171.48.0.144/32"]
  }
  tags = {
    Name = var.security_group_priv.sg_priv
  }
}
# Launching EC2 Instance
# Launching Bastion Host Instance
resource "aws_instance" "bastion_host" {
  ami                    = "ami-09e67e426f25ce0d7"
  key_name               = "aws"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.sg_bastion_host.id]
  availability_zone      = var.location.aws_az1
  subnet_id              = aws_subnet.subnet-public-1.id
  tags = {
    Name = var.bastion_host.bastion_host
  }
}
# Launching Private Instance -1 
resource "aws_instance" "priv-instance-1" {
  ami                    = "ami-09e67e426f25ce0d7"
  key_name               = "aws"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.sg_private_instance.id]
  availability_zone      = var.location.aws_az1
  subnet_id              = aws_subnet.subnet-private-1.id
  tags = {
    Name = var.priv-instance-1.priv-instance-1
  }
}
# Launching Private Instance -2
resource "aws_instance" "priv-instance-2" {
  ami                    = "ami-09e67e426f25ce0d7"
  key_name               = "aws"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.sg_private_instance.id]
  availability_zone      = var.location.aws_az2
  subnet_id              = aws_subnet.subnet-private-2.id
  tags = {
    Name = var.priv-instance-2.priv-instance-2
  }
}
# Launching Private Instance -3
resource "aws_instance" "priv-instance-3" {
  ami                    = "ami-09e67e426f25ce0d7"
  key_name               = "aws"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.sg_private_instance.id]
  availability_zone      = var.location.aws_az1
  subnet_id              = aws_subnet.subnet-private-1.id
  tags = {
    Name = var.priv-instance-3.priv-instance-3
  }
}
