output "main" {
  description = "VPC_ID"
  value       = aws_vpc.main.id
}
output "Subnet-Public-1" {
  description = "Subnet-Public-1-CIDR"
  value       = aws_subnet.subnet-public-1.id
}
output "Subnet-Public-2" {
  description = "Subnet-Public-2-CIDR"
  value       = aws_subnet.subnet-public-2.id
}
output "Subnet-Private-1" {
  description = "Subnet-Private-1-CIDR"
  value       = aws_subnet.subnet-private-1.id
}
output "Subnet-Private-2" {
  description = "Subnet-Private-2-CIDR"
  value       = aws_subnet.subnet-private-2.id
}
output "igw" {
  description = "IGW Id "
  value       = aws_internet_gateway.igw.id
}
output "nat" {
  description = "NAT Gateway Id "
  value       = aws_nat_gateway.nat.id
}
output "pub-rt" {
  description = "Public Route Table Id"
  value       = aws_route_table.pub-rt.id
}
output "priv-rt" {
  description = "Private Route Table Id"
  value       = aws_route_table.priv-rt.id
}
output "sg_bastion_host" {
  description = "Security group ID for Bastion Host "
  value       = aws_security_group.sg_bastion_host.id
}
output "sg_private" {
  description = "Security group ID for Private Instances"
  value       = aws_security_group.sg_private_instance.id
}
output "bastion_host" {
  description = "Bastion Host Id"
  value       = aws_instance.bastion_host.id
}
output "private-instance-1" {
  description = "Private Instance-1 Id"
  value       = aws_instance.priv-instance-1.id
}
output "private-instance-2" {
  description = "Private Instance-2 Id"
  value       = aws_instance.priv-instance-2.id
}
output "private-instance-3" {
  description = "Private Instance-3 Id"
  value       = aws_instance.priv-instance-3.id
}

